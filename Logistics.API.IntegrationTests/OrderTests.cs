using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using AutoFixture;
using Logistics.API.Models;
using NUnit.Framework;

namespace Logistics.API.IntegrationTests
{
    public class OrderTests
    {
        [OneTimeSetUp]
        public void InitializeServer()
        {
            _factory = new ApiWebApplicationFactory();
            _httpClient = _factory.CreateClient();
        }

        [SetUp]
        public void SetUp()
        {
            _fixture = new Fixture();
            _fixture.Customize<OrderItem>(c => c.With(item => item.ProductTypeId, RandomProductType));
        }
        
        [OneTimeTearDown]
        public void DisposeServer()
        {
            _factory.Dispose();
            _httpClient.Dispose();
        }

        [Test]
        public async Task PlaceOrderAndGetIt_ReturnSameOrder()
        {
            var placeOrderRequest = _fixture.Create<PlaceOrderRequest>();

            var placeOrderHttpResponse = await _httpClient.PostAsJsonAsync("orders", placeOrderRequest);
            var placeOrderResponseModel = await placeOrderHttpResponse.Content.ReadFromJsonAsync<OrderResponse>();
            var getOrderResponse =
                await _httpClient.GetFromJsonAsync<OrderResponse>("orders/" + placeOrderRequest.OrderId);
            
            Assert.AreEqual(placeOrderResponseModel.Id, getOrderResponse.Id);
            Assert.AreEqual(
                placeOrderResponseModel.PackageInfo.RequiredBinWidth,
                getOrderResponse.PackageInfo.RequiredBinWidth);
            Assert.AreEqual(placeOrderResponseModel.OrderItems.Length, getOrderResponse.OrderItems.Length);
            foreach (var (placeOrderOrderItem, getOrderOrderItem) in 
                placeOrderResponseModel.OrderItems.OrderBy(item => item.ProductTypeId)
                    .Zip(getOrderResponse.OrderItems.OrderBy(item => item.ProductTypeId)))
            {
                Assert.AreEqual(placeOrderOrderItem.ProductTypeId, getOrderOrderItem.ProductTypeId);
                Assert.AreEqual(placeOrderOrderItem.Quantity, getOrderOrderItem.Quantity);
            }
        }

        private string RandomProductType => _productTypeIds.OrderBy(_ => Guid.NewGuid()).First();
        
        private Fixture _fixture;
        private ApiWebApplicationFactory _factory;
        private HttpClient _httpClient;

        private readonly string[] _productTypeIds = {"photoBook", "mug", "cards", "calendar", "canvas"};
    }
}