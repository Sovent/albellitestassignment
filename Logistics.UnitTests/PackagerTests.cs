using System.Threading.Tasks;
using Logistics.Domain;
using Moq;
using NUnit.Framework;

namespace Logistics.UnitTests
{
    public class PackagerTests
    {
        [SetUp]
        public void Setup()
        {
            _productTypeRegistryMock = new Mock<IProductTypeRegistry>();
            _packager = new Packager(_productTypeRegistryMock.Object);
        }

        [Test]
        public async Task PackDifferentNonStackableProducts_PackageWidthIsSumOfProductsWidth()
        {
            SetProductTypes(
                new ProductType("id1", 2m, 1),
                new ProductType("id2", 5m, 1));
            var orderItems = new[]
            {
                new OrderItem("id1", 2),
                new OrderItem("id2", 1)
            };
            const decimal expectedPackageWidth = 2m * 2 + 5m * 1;

            var package = await _packager.Pack(orderItems);
            
            Assert.AreEqual(expectedPackageWidth, package.RequiredBinWidth);
        }

        [Test]
        public async Task PackProductsThatFitInOneStack_PackageWidthIsEqualToProductWidth()
        {
            SetProductTypes(new ProductType("id1", 2m, 2));
            var orderItems = new[]
            {
                new OrderItem("id1", 2)
            };
            const decimal expectedPackageWidth = 2m*1;

            var package = await _packager.Pack(orderItems);
            
            Assert.AreEqual(expectedPackageWidth, package.RequiredBinWidth);
        }
        
        [Test]
        public async Task PackProductsWithQuantityGreaterThanMaxStack_PackInMultipleStacks()
        {
            SetProductTypes(new ProductType("id1", 2m, 2));
            var orderItems = new[]
            {
                new OrderItem("id1", 3)
            };
            const decimal expectedPackageWidth = 2m*2;

            var package = await _packager.Pack(orderItems);
            
            Assert.AreEqual(expectedPackageWidth, package.RequiredBinWidth);
        }
        
        [Test]
        public async Task PackOrderItemsWithDuplicateStackableProduct_CombineThemInStack()
        {
            SetProductTypes(
                new ProductType("id1", 2m, 2), 
                new ProductType("id2", 5m, 1));
            var orderItems = new[]
            {
                new OrderItem("id1", 1),
                new OrderItem("id2", 1),
                new OrderItem("id1", 1)
            };
            const decimal expectedPackageWidth = 2m * 1 + 5m * 1;

            var package = await _packager.Pack(orderItems);
            
            Assert.AreEqual(expectedPackageWidth, package.RequiredBinWidth);
        }

        private void SetProductTypes(params ProductType[] productTypes)
        {
            foreach (var productType in productTypes)
            {
                _productTypeRegistryMock
                    .Setup(mock => mock.GetById(productType.Id))
                    .Returns(Task.FromResult(productType));
            }
        }

        private Packager _packager;
        private Mock<IProductTypeRegistry> _productTypeRegistryMock;
    }
}