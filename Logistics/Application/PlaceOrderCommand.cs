using System.Collections.Generic;
using FluentValidation;
using Logistics.Common;
using Logistics.Domain;

namespace Logistics.Application
{
    public class PlaceOrderCommand : IValidatable<PlaceOrderCommand, PlaceOrderCommand.PlaceOrderCommandValidator>
    {
        public PlaceOrderCommand(string orderId, IReadOnlyCollection<OrderItem> orderItems)
        {
            OrderId = orderId;
            OrderItems = orderItems;
            
            this.Validate();
        }

        public string OrderId { get; }
        
        public IReadOnlyCollection<OrderItem> OrderItems { get; }

        private class PlaceOrderCommandValidator : AbstractValidator<PlaceOrderCommand>
        {
            public PlaceOrderCommandValidator()
            {
                RuleFor(command => command.OrderId).NotEmpty();
                RuleForEach(command => command.OrderItems).ChildRules(orderItemValidator =>
                {
                    orderItemValidator.RuleFor(item => item.ProductTypeId).NotEmpty();
                    orderItemValidator.RuleFor(item => item.Quantity).GreaterThan(0);
                });
            }
        }
    }
}