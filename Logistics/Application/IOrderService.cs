using System.Threading.Tasks;
using Logistics.Domain;

namespace Logistics.Application
{
    public interface IOrderService
    {
        Task<Order> PlaceOrder(PlaceOrderCommand command);

        Task<Order> GetOrder(GetOrderQuery query);
    }
}