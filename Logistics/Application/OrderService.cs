using System.Threading.Tasks;
using Logistics.Domain;

namespace Logistics.Application
{
    public class OrderService : IOrderService
    {
        public OrderService(IOrderRepository orderRepository, IPackager packager)
        {
            _orderRepository = orderRepository;
            _packager = packager;
        }
        
        public async Task<Order> PlaceOrder(PlaceOrderCommand command)
        {
            var packageInfo = await _packager.Pack(command.OrderItems);
            var order = new Order(command.OrderId, command.OrderItems, packageInfo);
            await _orderRepository.Save(order);
            return order;
        }

        public async Task<Order> GetOrder(GetOrderQuery query)
        {
            return await _orderRepository.Get(query.OrderId);
        }
        
        private readonly IOrderRepository _orderRepository;
        private readonly IPackager _packager;
    }
}