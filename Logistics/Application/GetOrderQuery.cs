using FluentValidation;
using Logistics.Common;

namespace Logistics.Application
{
    public class GetOrderQuery : IValidatable<GetOrderQuery, GetOrderQuery.GetOrderQueryValidator>
    {
        public GetOrderQuery(string orderId)
        {
            OrderId = orderId;
            
            this.Validate();
        }

        public string OrderId { get; }

        private class GetOrderQueryValidator : AbstractValidator<GetOrderQuery>
        {
            public GetOrderQueryValidator()
            {
                RuleFor(query => query.OrderId).NotEmpty();
            }
        }
    }
}