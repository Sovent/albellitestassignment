using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using Logistics.Domain;

namespace Logistics.Infrastructure
{
    public class InMemoryOrderRepository : IOrderRepository
    {
        public Task<Order> Get(string orderId)
        {
            var order = _dictionary.GetValueOrDefault(orderId);
            if (order == default)
            {
                throw new OrderNotFound(orderId).ToException();
            }
            
            return Task.FromResult(order);
        }

        public Task Save(Order order)
        {
            _dictionary.AddOrUpdate(order.Id, order, (id, existingOrder) => order);
            return Task.CompletedTask;
        }

        private readonly ConcurrentDictionary<string, Order> _dictionary = new ConcurrentDictionary<string, Order>();
    }
}