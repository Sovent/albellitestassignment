using FluentValidation;
using Logistics.Common;

namespace Logistics.Domain
{
    public class OrderItem
    {
        public OrderItem(string productTypeId, int quantity)
        {
            ProductTypeId = productTypeId;
            Quantity = quantity;
        }

        public string ProductTypeId { get; }
        
        public int Quantity { get; }
    }
}