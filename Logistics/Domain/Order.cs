using System.Collections.Generic;

namespace Logistics.Domain
{
    public class Order
    {
        public Order(string id, IReadOnlyCollection<OrderItem> orderItems, PackageInfo packageInfo)
        {
            Id = id;
            OrderItems = orderItems;
            PackageInfo = packageInfo;
        }

        public string Id { get; }
        
        public IReadOnlyCollection<OrderItem> OrderItems { get; }
        
        public PackageInfo PackageInfo { get; }
    }
}