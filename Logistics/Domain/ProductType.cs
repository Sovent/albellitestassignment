namespace Logistics.Domain
{
    public class ProductType
    {
        public ProductType(string id, decimal stackWidthInMm, int maxPiecesInStack)
        {
            Id = id;
            StackWidthInMm = stackWidthInMm;
            MaxPiecesInStack = maxPiecesInStack;
        }

        public string Id { get; }
        
        public decimal StackWidthInMm { get; }
        
        public int MaxPiecesInStack { get; }
    }
}