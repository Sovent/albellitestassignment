using System.Threading.Tasks;

namespace Logistics.Domain
{
    public interface IProductTypeRegistry
    {
        Task<ProductType> GetById(string id);
    }
}