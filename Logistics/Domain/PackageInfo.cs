namespace Logistics.Domain
{
    public class PackageInfo
    {
        public PackageInfo(decimal requiredBinWidth)
        {
            RequiredBinWidth = requiredBinWidth;
        }

        public decimal RequiredBinWidth { get; }
    }
}