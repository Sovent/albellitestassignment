using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Logistics.Domain
{
    public class Packager : IPackager
    {
        public Packager(IProductTypeRegistry productTypeRegistry)
        {
            _productTypeRegistry = productTypeRegistry;
        }
        
        public async Task<PackageInfo> Pack(IReadOnlyCollection<OrderItem> orderItems)
        {
            var deduplicatedOrderItems = DeduplicateOrderItems(orderItems);
            
            var orderItemsWithProductTypes = await MatchWithProductTypes(deduplicatedOrderItems);

            var packageMinWidth = orderItemsWithProductTypes.Sum(tuple =>
            {
                var (orderItem, productType) = tuple;
                var stacksCount = Math.Ceiling((decimal) orderItem.Quantity / productType.MaxPiecesInStack);
                var minWidthInPackageForProduct = productType.StackWidthInMm * stacksCount;
                return minWidthInPackageForProduct;
            });

            return new PackageInfo(packageMinWidth);
        }

        private async Task<IEnumerable<(OrderItem OrderItem, ProductType ProductType)>> MatchWithProductTypes(
            IEnumerable<OrderItem> orderItems)
        {
            var orderItemsWithProductTypeTasks = orderItems
                .Select(item => (OrderItem: item, Task: _productTypeRegistry.GetById(item.ProductTypeId)))
                .ToArray();
            await Task.WhenAll(orderItemsWithProductTypeTasks.Select(tuple => tuple.Task));
            return orderItemsWithProductTypeTasks.Select(tuple => (tuple.OrderItem, ProductType: tuple.Task.Result));
        }

        private static IEnumerable<OrderItem> DeduplicateOrderItems(IEnumerable<OrderItem> orderItems)
        {
            return orderItems
                .ToLookup(item => item.ProductTypeId)
                .Select(grouping => new OrderItem(grouping.Key, grouping.Sum(item => item.Quantity)));
        }
        
        private readonly IProductTypeRegistry _productTypeRegistry;
    }
}