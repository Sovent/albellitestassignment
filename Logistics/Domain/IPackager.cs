using System.Collections.Generic;
using System.Threading.Tasks;

namespace Logistics.Domain
{
    public interface IPackager
    {
        Task<PackageInfo> Pack(IReadOnlyCollection<OrderItem> orderItems);
    }
}