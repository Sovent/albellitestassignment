using System.Threading.Tasks;

namespace Logistics.Domain
{
    public interface IOrderRepository
    {
        Task<Order> Get(string orderId);

        Task Save(Order order);
    }
}