using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Logistics.Domain
{
    public class PredefinedProductTypeRegistry : IProductTypeRegistry
    {
        public Task<ProductType> GetById(string id)
        {
            var productType = _productTypes.GetValueOrDefault(id);
            if (productType == default)
            {
                throw new ProductTypeNotFound(id).ToException();
            }
            
            return Task.FromResult(productType);
        }

        private readonly Dictionary<string, ProductType> _productTypes = new[]
        {
            new ProductType("photoBook", stackWidthInMm: 19, maxPiecesInStack: 1),
            new ProductType("calendar", 10, 1),
            new ProductType("canvas", 16, 1),
            new ProductType("cards", 4.7m, 1),
            new ProductType("mug", 94m, 4)
        }.ToDictionary(type => type.Id);
    }
}