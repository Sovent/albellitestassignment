using Logistics.Common;

namespace Logistics.Domain
{
    public class ProductTypeNotFound : DomainError<ProductTypeNotFound>
    {
        public ProductTypeNotFound(string productTypeId)
        {
            Description = $"Product type with id '{productTypeId}' not found";
        }
        
        public override string Description { get; }
    }
}