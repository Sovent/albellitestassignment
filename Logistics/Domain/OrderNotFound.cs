using Logistics.Common;

namespace Logistics.Domain
{
    public class OrderNotFound : DomainError<OrderNotFound>
    {
        public OrderNotFound(string orderId)
        {
            Description = $"Order with id '{orderId}' not found";
        }
        
        public override string Description { get; }
    }
}