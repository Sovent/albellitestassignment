using System.Threading.Tasks;
using AutoMapper;
using Logistics.API.Models;
using Logistics.Application;
using Microsoft.AspNetCore.Mvc;

namespace Logistics.API.Controllers
{
    [ApiController]
    [Route("orders")]
    public class OrderController : ControllerBase
    {
        public OrderController(IOrderService orderService, IMapper mapper)
        {
            _orderService = orderService;
            _mapper = mapper;
        }
        
        [HttpPost]
        public async Task<IActionResult> PlaceOrder([FromBody] PlaceOrderRequest request)
        {
            var command = _mapper.Map<PlaceOrderCommand>(request);
            var order = await _orderService.PlaceOrder(command);
            var responseModel = _mapper.Map<OrderResponse>(order);
            return Ok(responseModel);
        }

        [HttpGet]
        [Route("{orderId}")]
        public async Task<IActionResult> GetOrder(string orderId)
        {
            var query = new GetOrderQuery(orderId);
            var order = await _orderService.GetOrder(query);
            var responseModel = _mapper.Map<OrderResponse>(order);
            return Ok(responseModel);
        }
        
        private readonly IOrderService _orderService;
        private readonly IMapper _mapper;
    }
}