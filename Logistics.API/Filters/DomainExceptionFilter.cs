using System.Net;
using Logistics.API.Models;
using Logistics.Common;
using Logistics.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Logistics.API.Filters
{
    public class DomainExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            void SetResult<T>(DomainException<T> exception, HttpStatusCode statusCode) where T : DomainError<T>
            {
                context.Result = new ObjectResult(
                    new ErrorResponse
                    {
                        ErrorDescription = exception.Error.Description
                    })
                {
                    StatusCode = (int) statusCode
                };
                context.ExceptionHandled = true;
            }

            switch (context.Exception)
            {
                case DomainException<ValidationError> exception:
                    SetResult(exception, HttpStatusCode.BadRequest);
                    break;
                case DomainException<OrderNotFound> exception:
                    SetResult(exception, HttpStatusCode.NotFound);
                    break;
                case DomainException<ProductTypeNotFound> exception:
                    SetResult(exception, HttpStatusCode.NotFound);
                    break;
            }
        }
    }
}