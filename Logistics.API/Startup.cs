using AutoMapper;
using Logistics.API.Filters;
using Logistics.Application;
using Logistics.Domain;
using Logistics.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Logistics.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddControllers(options =>
                {
                    options.Filters.Add<ModelValidationFilter>();
                    options.Filters.Add<DomainExceptionFilter>();
                })
                .ConfigureApiBehaviorOptions(options => options.SuppressModelStateInvalidFilter = true);

            services.AddSingleton<IOrderService, OrderService>();
            services.AddSingleton<IPackager, Packager>();
            services.AddSingleton<IProductTypeRegistry, PredefinedProductTypeRegistry>();
            services.AddSingleton<IOrderRepository, InMemoryOrderRepository>();

            services.AddAutoMapper(configuration => { configuration.AddProfile(typeof(MappingProfile)); });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IMapper mapper)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

            mapper.ConfigurationProvider.AssertConfigurationIsValid();
        }
    }
}