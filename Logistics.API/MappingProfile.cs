using AutoMapper;
using Logistics.API.Models;
using Logistics.Application;
using Logistics.Domain;
using OrderItem = Logistics.Domain.OrderItem;
using PackageInfo = Logistics.Domain.PackageInfo;

namespace Logistics.API
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<PlaceOrderRequest, PlaceOrderCommand>();
            CreateMap<Models.OrderItem, OrderItem>();
            CreateMap<OrderItem, Models.OrderItem>();
            CreateMap<Order, OrderResponse>();
            CreateMap<PackageInfo, Models.PackageInfo>();
        }
    }
}