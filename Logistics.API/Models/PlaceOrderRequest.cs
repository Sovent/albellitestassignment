namespace Logistics.API.Models
{
    public class PlaceOrderRequest
    {
        public string OrderId { get; set; }
        
        public OrderItem[] OrderItems { get; set; }
    }
}