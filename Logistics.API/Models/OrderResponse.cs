namespace Logistics.API.Models
{
    public class OrderResponse
    {
        public string Id { get; set; }
        
        public OrderItem[] OrderItems { get; set; }
        
        public PackageInfo PackageInfo { get; set; }
    }
}