namespace Logistics.API.Models
{
    public class OrderItem
    {
        public string ProductTypeId { get; set; }
        
        public int Quantity { get; set; }
    }
}